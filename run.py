# coding=utf-8
from flask import Flask, render_template, request
app = Flask(__name__)


class Item():
    def __init__(self, name, amount):
        self.name = name
        self.amount = amount


@app.route("/")
def hello():
    # items = [
    #    Item("Apfel", 5),
    #    Item("Computer", 1),
    #    Item("Birne", 4)
    # ]

    items = [
        {"name": "Apfel", "amount": 5},
        {"name": "Computer", "amount": 1},
        {"name": "Birne", "amount": 4}
    ]

    for item in items:
        item["amount"] = item["amount"] * 2

    person = ("Max", "Mustermann")

    return render_template("start.html", person=person, items=items)


@app.route("/test")
def test():
    name = request.args.get("name")
    age = request.args.get("age")
    return render_template("test.html", name=name, age=age)


@app.route("/currency")
def currency():
    curr_1, curr_2 = request.args.get("curr_1", "EUR"), request.args.get("curr_2", "USD")
    rate = float(request.args.get("rate", 0.9046))
    multiples, multiples2 = [], []
    multiples.append((1, round(rate, 2)))
    multiples2.append((round(1 / rate, 2), 1))

    for i in range(1, 11):
        multiples.append((i * 10, round(i * 10 * rate, 2)))
        multiples2.append((round(i * 10 * (1 / rate), 2), i * 10))

    return render_template("currency.html", curr_1=curr_1, curr_2=curr_2, rate=rate,
                           multiples=multiples, multiples2=multiples2)
